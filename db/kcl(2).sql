-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 10, 2014 at 10:14 AM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `kcl`
--
CREATE DATABASE IF NOT EXISTS `kcl` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `kcl`;

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE IF NOT EXISTS `employee` (
  `emp_id` varchar(20) NOT NULL DEFAULT '',
  `emp_name` varchar(200) NOT NULL,
  `emp_rank` int(4) NOT NULL,
  `emp_speech` varchar(500) NOT NULL,
  `skills_lang` varchar(120) NOT NULL,
  `skills_frame` varchar(100) NOT NULL,
  `self_profile` varchar(100) NOT NULL,
  `self_blog` varchar(100) NOT NULL,
  `kcl_id` int(11) NOT NULL,
  PRIMARY KEY (`emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE IF NOT EXISTS `project` (
  `project_id` int(10) NOT NULL AUTO_INCREMENT,
  `code` varchar(20) NOT NULL,
  `project_name` varchar(200) NOT NULL,
  `rank` int(10) NOT NULL,
  `project_lang` varchar(200) NOT NULL,
  `project_frame` varchar(200) NOT NULL,
  `project_description` text NOT NULL,
  `project_remarks` varchar(500) NOT NULL,
  PRIMARY KEY (`project_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`project_id`, `code`, `project_name`, `rank`, `project_lang`, `project_frame`, `project_description`, `project_remarks`) VALUES
(1, 'DPM', 'Digital Print Module', 1, 'node.js , JavaScript,HTML,CSS.', 'Twitter Bootstrap , JQChart,jQuery keyboard. ', 'It is a real time update project . Here we have use huge number of scoket.io . It is a real time update project . Here we have use huge number of scoket.io It is a real time update project . Here we have use huge number of scoket.io ', 'certified by MA'),
(2, 'MI', 'Markendy Web App', 2, 'node.js , JavaScript,HTML,CSS.', 'Twitter Bootstrap , JQChart,jQuery keyboard. ', 'It is a real time update project . Here we have use huge number of scoket.io . ', 'certified by MA');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
