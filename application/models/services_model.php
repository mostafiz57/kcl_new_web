<?php

class services_model extends CI_Model {
    /*
     * A private variable to represent each column in the database
     */

    private $_id;
    private $_code;
    private $_frame;
    private $_lang;
    private $_description;
    private $_remarks;
    private $_name;

    function __construct() {
        parent::__construct();
        $this->load->library("ProjectFactory");
    }

    /*
     * SET's & GET's
     * Set's and get's allow you to retrieve or set a private variable on an object
     */

    public function getId() {
        return $this->_id;
    }

    public function setId($value) {
        $this->_id = $value;
    }

    /* Project Name */

    public function getName() {
        return $this->_name;
    }

    public function setName($value) {
        $this->_name = $value;
    }

    /* Project Code */

    public function getCode() {
        return $this->_code;
    }

    public function setCode($value) {
        $this->_code = $value;
    }

    /* Project Framework */

    public function getFrame() {
        return $this->_frame;
    }

    public function setFrame($value) {
        $this->_frame = $value;
    }

    /* Project Language */

    public function getLang() {
        return $this->_lang;
    }

    public function setLang($value) {
        $this->_lang = $value;
    }

    /* Project Description */

    public function getDescription() {
        return $this->_description;
    }

    public function setDescription($value) {
        $this->_description = $value;
    }

    /* Project Remarks */

    public function getRemarks() {
        return $this->_remarks;
    }

    public function setRemarks($value) {
        $this->_remarks = $value;
    }

    function getPortfolioData($data = 0) {

        //Always ensure an integer
        $projectId = (int) $data;
        //Load the user factory
        //Create a data array so we can pass information to the view
        $projectData = array(
            "projects" => $this->projectfactory->getProject($projectId)
        );
        return $projectData;
    }

}