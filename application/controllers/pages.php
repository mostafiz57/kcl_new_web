<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pages extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('services_model');
    }

    public function view($page = 'home') {

        if (!file_exists(APPPATH . '/views/pages/' . $page . '.php')) {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $data['title'] = ucfirst($page); // Capitalize the first letter
        $this->load->library('email');
        if ($this->uri->segment(2) != null) {
            if ($this->uri->segment(2) == 'sendEmail') {
                $sender_name = $_POST['visitor_email'];
                $sender_email = $_POST['visitor_name'];
                $sender_phone = $_POST['visitor_phone'];
                $sender_message = $_POST['visitor_message'];
                $receiver_email = 'mostafiz57@gmail.com';

                $this->email->from($sender_email, $sender_name);
                $this->email->to($receiver_email);

                $this->email->subject('Email Testing');
                $this->email->message($sender_message);

                $this->email->send();
                /* $email_config = Array(
                  'protocol' => 'smtp',
                  'smtp_host' => 'ssl://smtp.googlemail.com',
                  'smtp_port' => '465',
                  'smtp_user' => 'codinglikeasir@gmail.com',
                  'smtp_pass' => 'MySirPassword',
                  'mailtype' => 'html',
                  'starttls' => true,
                  'newline' => "\r\n"
                  );

                  $this->load->library('email', $email_config);

                  $this->email->from('sir@codinglikeasir.com', 'A. Sir');
                  $this->email->to('mostafiz57@gmail.com');

                  $this->email->subject('Sire Subject');
                  $data = array('message' => "Hello Sir, you've got mail!");
                  $email = $this->load->view('index', $data, TRUE);

                  $this->email->message($email);
                  $this->email->send();
                  echo 'email send successfully'; */
            }
            if ($this->uri->segment(2) == 'projectDescription') {
                $portfolioData = array();
                $portfolioData = $this->services_model->getPortfolioData($_POST['id']);
                echo json_encode($portfolioData);
                return $portfolioData;
            }
        } else {
            if ($this->uri->segment(1) == 'portfolio') {
                $portfolioData = array();
                $portfolioData = $this->services_model->getPortfolioData(0);
                $this->load->view('templates/header', $data);
                $this->load->view('pages/' . $page, $portfolioData);
                $this->load->view('templates/footer', $data);
            } else {
                $this->load->view('templates/header', $data);
                $this->load->view('pages/' . $page, $data);
                $this->load->view('templates/footer', $data);
            }
        }
    }

}
