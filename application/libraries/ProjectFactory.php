<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class ProjectFactory {

	private $_ci;

 	function __construct()
    {
    	//When the class is constructed get an instance of codeigniter so we can access it locally
    	$this->_ci =& get_instance();
    	//Include the service_model so we can use it
    	$this->_ci->load->model("services_model");
    }

    public function getProject($id = 0) {
    	//Are we getting an individual user or are we getting them all
    	if ($id > 0) {
    		//Getting an individual project
    		$query = $this->_ci->db->get_where("project", array("project_id" => $id));
    		//Check if any results were returned
    		if ($query->num_rows() > 0) {
    			//Pass the data to our local function to create an object for us and return this new object
    			return $query->row();
    		}
    		return false;
    	} else {
    		//Getting all the project
    		$query = $this->_ci->db->select("*")->from("project")->get();
    		//Check if any results were returned
    		if ($query->num_rows() > 0) {
    			//Create an array to store users
    			$projects = array();
    			//Loop through each row returned from the query
    			foreach ($query->result() as $row) {
    				//Pass the row data to our local function which creates a new user object with the data provided and add it to the users array
    				$projects[] = $this->createObjectFromData($row);
    			}
    			//Return the users array
    			return $projects;
    		}
    		return false;
    	}
    }

    public function createObjectFromData($row) {
    	//Create a new user_model object
    	$projects = new Services_Model();
    	//Set the ID on the user model
    	$projects->setId($row->project_id);
    	//Set the code on the project model
    	$projects->setCode($row->code);
        //Set the code on the project model
    	$projects->setName($row->project_name);
    	//Set the Framework on the project model
    	$projects->setFrame($row->project_frame);
        //Set the Language on the project model
    	$projects->setLang($row->project_lang);
        //Set the Descriptiion on the project model
    	$projects->setDescription($row->project_description);
         //Set the Remarks on the project model
    	$projects->setRemarks($row->project_remarks);
    	//Return the new user object
    	return $projects;
    }

}