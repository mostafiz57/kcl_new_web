<script>
    menuId = 'home';
</script>   
<div class="row slide-show">
    <div id="myCarousel" class="carousel slide">
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>
        <!-- Carousel items -->
        <div class="carousel-inner">
            <div class="active item">
                <div class="leftcontaint">
                    <input type="image" src="<?php echo base_url(); ?>/assets/img/slideshow/about_company.jpg" width="100%" height="400">
                </div>
                <div class="rightcontaint">
                   
                </div>
            </div>
            <div class="item">
                <div class="leftcontaint">
                    <input type="image" src="<?php echo base_url(); ?>/assets/img/slideshow/featured_service_1.PNG" 
                           width="100%" height="400">
                </div>
                <div class="rightcontaint">
                   

                </div>
            </div>
            <div class="item">
                <div class="leftcontaint">
                    <input type="image" src="<?php echo base_url(); ?>/assets/img/slideshow/portfolio_1.PNG" width="100%" height="400">
                </div>
                <div class="rightcontaint">
                    
                </div>
            </div>
        </div>
        <!-- Carousel nav -->
        <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
        <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
    </div>
</div>

<div class="row advantages-mission-vision">
    <div class="col-md-3 advantages">
        <div class="row amv-heading">VALUES</div>
        <div class="row amv-title"></div>
        <div class="row amv-content">
            <b>Long success: </b>We seek long-term success for our organization by bringing in success... <br>
            <span id="values"  onclick="populateHomeContent(this)" class="row portfolio-more pointer more">more...</span>
        </div>
        <!-- <button class="btn btn-primary amv-more-btn">MORE</button> -->
    </div>
    <div class="col-md-3 col-md-offset-1 mission">
        <div class="row amv-heading">MISSION</div>
        <div class="row amv-title"></div>
        <div class="row amv-content"> Our mission today is made up of our vision, values, business strategy and brand promise...</div>
         <span id="mission"  onclick="populateHomeContent(this)" class="row portfolio-more pointer more">more...</span>
        <!-- <button class="btn btn-primary amv-more-btn">MORE</button> -->
    </div>
    <div class="col-md-3 col-md-offset-1 vision">
        <div class="row amv-heading">VISION</div>
        <div class="row amv-title"></div>
        <div class="row amv-content">To be one of the most valued and respected global mid -sized Technology Companies</div>
        <!-- <button class="btn btn-primary amv-more-btn">MORE</button> -->
    </div>
</div>

<div class="row how-we-works-services">
    <div class="col-md-7">
        <div class="row how-we-works-heading">WE DO IT ALL</div>
        <div class="row working-steps">
            <div id="tabs">
                <ul>
                    <li><a href="#tabs-1">Software development</a></li>
                    <li><a href="#tabs-2">Data Content & Research</a></li>
                    <li><a href="#tabs-3">Quality Assurance</a></li>
                </ul>
                <div id="tabs-1">
                    <div class="row step-content-div-tag">
                        <div class="col-md-4"><img src="<?php echo base_url(); ?>/assets/img/working_steps/step1.jpg" alt="step 1" 
                                                   class="img-thumbnail"></div>
                        <div class="col-md-8 step-text-div-tag">Our software teams help our customers build  customized software - 
                            everything from web to desktop to enterprise to mobile and beyond.  
                            We hire only the best. Our culture helps us retain our talents and our skill development programs ensure
                            that we are always on the top of recent developments...</div>
                    </div>
                </div>
                <div id="tabs-2">
                    <div class="row">
                        <div class="col-md-4"><img src="<?php echo base_url(); ?>/assets/img/working_steps/step2.gif" alt="step 2" 
                                                   class="img-thumbnail"></div>
                        <div class="col-md-8 step-text-div-tag">Once the software is done, content is king. We understand this need
                            and help our customers building up their content. Our research teams have researched, compiled and 
                            maintained content in diverse fields and for a variety of applications.</div>
                    </div>
                </div>
                <div id="tabs-3">
                    <div class="row">
                        <div class="col-md-4"><img src="<?php echo base_url(); ?>/assets/img/working_steps/step3.jpg" alt="step 3" 
                                                   class="img-thumbnail"></div>
                        <div class="col-md-8 step-text-div-tag">Great design and development goes nowhere without great quality. 
                            Our integrated quality assurance approach incorporates aspects of agile and lean development with the
                            stability and reliability of traditional SQA process.</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4 col-md-offset-1">
        <div class="row service-heading">SERVICES</div>
        <div class="row">
            <ul class="service-list">
                <li><a href="<?php echo base_url();?>/index.php/services?isService=1">like to better serve to our customers</a></li>
                <li><a href="<?php echo base_url();?>/index.php/services?isService=1">Like to improve your product</a></li>
                <li><a href="<?php echo base_url();?>/index.php/services?isService=1">Professional Services</a></li>
                <li><a href="<?php echo base_url();?>/index.php/services?isService=1">like to communicate with our customers</a></li>
                <li><a href="<?php echo base_url();?>/index.php/services?isService=1">like to provide top technology</a></li>
                <li><a href="<?php echo base_url();?>/index.php/services?isService=1">develop suited product</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div id="modalBody"  class="modal-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
