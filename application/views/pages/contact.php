<script>
menuId='contact';
</script>        
<div class="row breadcrumb-div-tag">
          <h1 class="breadcrumb-heading">CONTACT</h1>
          <ol class="breadcrumb">
            <li><a href="index.html">Home</a></li>
            <li class="active">Contact</li>
          </ol>
        </div>
        <div class="row site-map" id="map_canvas"></div>

        <div class="row contact-div-tag">
            <div class="col-md-4 contact-info-div">
              <div class="row contact-heading">CONTACT INFO</div>
              <div class="row contact-address">
                <span style="color:#45A6F0;">Tell us how we can help ?</span><br><br>
                <span style="font-weight: bold;">Kernel Coders Lab</span><br>
                <span>House# 462/B, Sector 1, Uposhohor Housing State, Rajshahi-6203, Bangladesh.</span><br>
                <span>Phone: 01719404134, 01676660807</span><br>
                <span>email: <span style="color:#45A6F0; text-transform:lowercase;"><a href="kernel.writer@gmail.com">kernel.writer@gmail.com</a></span></span>
              </div>
            </div>
            <div class="col-md-7 col-md-offset-1 contact-form-div">
              <div class="row contact-heading">CONTACT FORM</div>
              <div class="row contact-form">
                <form id="emailForm" action="<?php echo base_url(); ?>index.php/contact/sendEmail" method="Post">
                  <input type="text" name="visitor_name" placeholder="NAME:" title="Name" class="contact-form-fld">
                  <input type="text" name="visitor_email" placeholder="E-MAIL:" title="Email" class="contact-form-fld">
                  <input type="text" name="visitor_phone" placeholder="PHONE:" title="Phone" class="contact-form-fld">
                  <textarea name="visitor_message" placeholder="MESSAGE:" class="contact-form-msg-fld" title="Message"></textarea>
                  <input type="reset" value="CLEAR" class="btn btn-primary contact-submit-btn">
                  <input type="button" id="sendEmail" value="SEND" class="btn btn-primary contact-submit-btn">
                </form>
              </div>
            </div>
        </div>


