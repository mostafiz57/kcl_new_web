<script>
menuId='services';
if(qs('isService') == 1)
    $('html, body').animate({ scrollTop:880 }, 'slow');

function qs(key) {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars[key];
}

</script>   
<div class="row breadcrumb-div-tag">
    <h1 class="breadcrumb-heading">SERVICES</h1>
    <ol class="breadcrumb">
        <li><a href="index.html">Home</a></li>
        <li class="active">Services</li>
    </ol>
</div>

<div class="row service-div-tag">
    <div class="col-md-4 service-list-div">
        <div class="row service-heading">services list</div>
        <div class="row service-name">
            <ul class="service-list">
                <li><a href="#">like to better serve to our customers</a></li>
                <li><a href="#">Like to improve your product</a></li>
                <li><a href="#">Professional Services</a></li>
                <li><a href="#">like to communicate with our customers</a></li>
                <li><a href="#">like to provide top technology</a></li>
                <li><a href="#">develop suited product</a></li>
            </ul>
        </div>
    </div>
    <div class="col-md-8 featured-service-div">
        <div class="row service-heading">featured services</div>
        <div class="row">
            <div class="col-md-6 featured-service-content">
                <span class="row portfolio-img"><img src="<?php echo base_url(); ?>/assets/img/featured_services/featured_service_1.png" class="img-thumbnail" width="100%"></span>
                <span class="row portfolio-heading">--</span>
                <span class="row portfolio-text">--</span>
            </div>
            <div class="col-md-6 featured-service-content">
                <span class="row portfolio-img"><img src="<?php echo base_url(); ?>/assets/img/featured_services/featured_service_2.png" class="img-thumbnail" width="100%"></span>
                <span class="row portfolio-heading">--</span>
                <span class="row portfolio-text">--</span>
            </div>
        </div>
    </div>
</div>

<div class="row service-overview-div-tag">
    <div class="row">
        <div class="col-md-12 service-overview-heading">services overview</div>
    </div>
    <div class="row service-content">
        <div class="col-md-4 sevice-single-overview">
            <div class="row">
                <div class="col-md-3 service-img"><img src="<?php echo base_url(); ?>/assets/img/services/service_1.png" class="img-rounded"></div>
                <div class="col-md-8 service-text"><span class="service-overview-name">like to better serve to our customers</span><br>Customer service is important to us. Our customer service solutions are designed with your business in mind to help you better serve your customers through any channel and at any time in the business cycle.</div>
            </div>
        </div>
        <div class="col-md-4 sevice-single-overview">
            <div class="row">
                <div class="col-md-3 service-img"><img src="<?php echo base_url(); ?>/assets/img/services/service_2.png" class="img-rounded"></div>
                <div class="col-md-8 service-text"><span class="service-overview-name">Like to improve your product</span><br>We are committed and always ready  to improve your products in future that your business needed.</div>
            </div>
        </div>
        <div class="col-md-4 sevice-single-overview">
            <div class="row">
                <div class="col-md-3 service-img"><img src="<?php echo base_url(); ?>/assets/img/services/service_3.png" class="img-rounded"></div>
                <div class="col-md-8 service-text"><span class="service-overview-name">Professional Services</span><br>Professional Services of our company  provides the right expertise— right now and in the right way— so you can focus on your business, while we facilitate each aspect of your project. Our service professionals have years of experience in the management, development, and support of large and complex systems. Our consulting services include Project Management, Solutions Implementation, Technical Services, Quality Assurance, Database Administration, Custom Development, and Learning Solutions. </div>
            </div>
        </div>
        <div class="col-md-4 sevice-single-overview">
            <div class="row">
                <div class="col-md-3 service-img"><img src="<?php echo base_url(); ?>/assets/img/services/service_4.png" class="img-rounded"></div>
                <div class="col-md-8 service-text"><span class="service-overview-name">like to communicate with our customers</span><br>We will continually communicate with, and learn from our Customers, in order to improve our products and services.</div>
            </div>
        </div>
        <div class="col-md-4 sevice-single-overview">
            <div class="row">
                <div class="col-md-3 service-img"><img src="<?php echo base_url(); ?>/assets/img/services/service_5.png" class="img-rounded"></div>
                <div class="col-md-8 service-text"><span class="service-overview-name">like to provide top technology</span><br>We like and keep on top of today's and tomorrow's technology, no matter how fast it moves, to ensure our Customers always have the best tools available to them.</div>
            </div>
        </div>
        <div class="col-md-4 sevice-single-overview">
            <div class="row">
                <div class="col-md-3 service-img"><img src="<?php echo base_url(); ?>/assets/img/services/service_6.png" class="img-rounded"></div>
                <div class="col-md-8 service-text"><span class="service-overview-name">develop suited product</span><br>We are developing  those products with your business in mind so they’re entirely suited to your needs.</div>
            </div>
        </div>
    </div>
</div>