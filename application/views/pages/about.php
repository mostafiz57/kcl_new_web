<script>
    menuId = 'about';
</script>  
<div class="row breadcrumb-div-tag">
    <h1 class="breadcrumb-heading">ABOUT</h1>
    <ol class="breadcrumb">
        <li><a href="index.html">Home</a></li>
        <li class="active">About</li>
    </ol>
</div>

<div class="row about-div-tag">
    <div class="col-md-7 about-our-company">
        <div class="row contact-heading">ABOUT OUR COMPANY</div>
        <div class="row company-content">
            <div class="col-md-4 company-img"><img src="<?php echo base_url(); ?>/assets/img/about_company.jpg" class="img-thumbnail"></div>
            <div class="col-md-8 company-text-1"><span style="color:#3276B1;">KERNEL CODERS LAB</span><br>Kernel Coders Lab is a Rajshahi,Bangladesh based software outsourcing 
                company that focuses on highly qualitative, timely delivered and cost-effective offshore software development. With a rich and varied experience in 
                providing offshore software development and project management capabilities and stringent quality standards ensure us to 
                develop solutions that give your business an edge over your competitors. Our global software outsourcing model makes sure
                we deliver maximum targeted result to YOU.</div>
            <span class="company-text-2"><i>“We are always on startup mode which ever project we do - our passion drives us, our open 
                    culture brings out creativity and innovation and our strong skill base and experience coupled with our tried and 
                    tested development process makes great software a reality.
                We believe we are good at what we do .”</i></span>
            <span class="company-director-title"></span>
            <span class="company-director-details"></span>
        </div>
    </div>
    <div class="col-md-5 what-we-offer">
        <div class="row contact-heading">OUR TECHNOLOGIES</div>
        <div class="row offer-content">
            <div class="col-md-2"><img src="<?php echo base_url(); ?>/assets/img/offer-1.png" class="img-rounded"></div>
            <div class="col-md-9 col-md-offset-1">
                <span class="offer-title">Desktop & Embedded</span>
                <span class="offer-details"><b>Languages: </b>C/C++, C#/VB<b><br>Frameworks: </b>Qt/QML, .NET/WPF/WinForm<br></span>
            </div>
        </div>
        <div class="row offer-content">
            <div class="col-md-2"><img src="<?php echo base_url(); ?>/assets/img/offer-2.png" class="img-rounded"></div>
            <div class="col-md-9 col-md-offset-1">
                <span class="offer-title">Web Development</span>
                <span class="offer-details"><b>Languages: </b> C#, PHP, Javascript<b><br>Frameworks: </b>Node.js, Anguler.js, Backbone.js, Zend, Codegniter, CakePHP, ASP.NET, Bootstrap/Foundation/LESS<br><b>New Technologies: </b>HTML5, WebRTC<br></span>                
            </div>
        </div>
        <div class="row offer-content">
            <div class="col-md-2"><img src="<?php echo base_url(); ?>/assets/img/offer-3.png" class="img-rounded"></div>
            <div class="col-md-9 col-md-offset-1">
                <span class="offer-title">Mobile Development</span>
                <span class="offer-details">Android/Java, iOS/Objective-C/C++, Windows Phone</span>
            </div>
        </div>
        <div class="row offer-content">
            <div class="col-md-2"><img src="<?php echo base_url(); ?>/assets/img/offer-4.png" class="img-rounded"></div>
            <div class="col-md-9 col-md-offset-1">
                <span class="offer-title">Databases</span>
                <span class="offer-details"><b>Relational: </b>MSSQL Server, MySQL, SQLite<b><br>NoSQL: </b> MongoDB, CouchDB, Redis<br></span>
            </div>
        </div>
    </div>
</div>

<div class="row testimonial-heading">testimonials</div>
<div class="row">
    <div class="row testimonial-content">
        <div class="col-md-4">
            <a href="#" class="testimonial" title="Testimonial# 1">
                <span class="testimonial-text">"Aenean nonummy hendrerit mau phasellu porta. Fusce suscipit varius mi sed. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nulla dui. Fusce feugiat malesuada odio morbi. Ut viverra mauris justo, quis auctor nisi. Suspendisse sit amet... "</span>
                <span class="testimonial-client"><i>Laura Stegner, client</i></span>
            </a>
        </div>
        <div class="col-md-4">
            <a href="#" class="testimonial" title="Testimonial# 2">
                <span class="testimonial-text">"Aenean nonummy hendrerit mau phasellu porta. Fusce suscipit varius mi sed. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nulla dui. Fusce feugiat malesuada odio morbi. Ut viverra mauris justo, quis auctor nisi. Suspendisse sit amet... "</span>
                <span class="testimonial-client"><i>Laura Stegner, client</i></span>
            </a>
        </div>
        <div class="col-md-4">
            <a href="#" class="testimonial" title="Testimonial# 3">
                <span class="testimonial-text">"Aenean nonummy hendrerit mau phasellu porta. Fusce suscipit varius mi sed. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nulla dui. Fusce feugiat malesuada odio morbi. Ut viverra mauris justo, quis auctor nisi. Suspendisse sit amet... "</span>
                <span class="testimonial-client"><i>Laura Stegner, client</i></span>
            </a>
        </div>
    </div>
    <a href="#" target="_blank"><button title="Read all testimonials" class="btn btn-primary all-testimonial-btn">READ ALL TESTIMONIALS</button></a>
</div>