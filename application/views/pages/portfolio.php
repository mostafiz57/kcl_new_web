<script>
    menuId = 'portfolio';
</script>
<?php
//Create the HTML table header
echo <<<HTML
<div class="row breadcrumb-div-tag">
    <h1 class="breadcrumb-heading">PORTFOLIO</h1>
    <ol class="breadcrumb">
        <li><a href="index.html">Home</a></li>
        <li class="active">Portfolio</li>
    </ol>
</div>

<div class="row portfolio-category">
    <div class="col-md-2 portfolio-category-heading">categories</div>
    <div class="col-md-5 portfolio-category-tablist">
        <ul>
            <li>show all</li>
            <li>web application</li>
            <li>software development</li>
        </ul>
    </div>
</div>
<div class="row">
    <div class="row portfolio-content">

HTML;

//Do we have an array of users or just a single user object?
$basicUrl = base_url();
foreach ($projects as $project) {
    $img=$project->getCode();
    $projectID=$project->getId();
    echo <<<HTML
<div class="col-md-4">
                <span class="row portfolio-img"><img src="{$basicUrl}/assets/img/portfolio/{$img}.png" class="img-thumbnail"></span>
                <span class="row portfolio-heading">Project Name : {$project->getName()}</span>
                <span class="row portfolio-text">
                  Language :{$project->getLang()}
                 </span>
                  <span class="row portfolio-text">
                  Framework :{$project->getFrame()}
                 </span>
                <span id=id{$projectID}  onclick="popupPortfolio({$projectID})" url="{$basicUrl}index.php/portfolio/projectDescription" class="row portfolio-more pointer more">more...</span>
            </div>
                  
 <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div id="modalBody"  class="modal-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

HTML;
}

//Close the table HTML
echo <<<HTML
		 </div>
</div>
HTML;
?>

