<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>kernelcoderslab -<?php echo $title ?> </title>
        <link rel="SHORTCUT ICON" href="<?php echo base_url(); ?>/assets/img/favicon.ico" type="image/x-icon" />
        <link rel="ICON" href="<?php echo base_url(); ?>/assets/img/favicon.ico" type="image/ico" />
        <link href="<?php echo base_url(); ?>/assets/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <link href='http://fonts.googleapis.com/css?family=Josefin+Sans:700' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Muli' rel='stylesheet' type='text/css'>
        <script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/library/jquery-1.10.2.js"></script>
        <link href="<?php echo base_url(); ?>/assets/css/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url(); ?>/assets/css/style.css" rel="stylesheet" type="text/css">
        <script src="<?php echo base_url(); ?>/assets/js/library/jquery-ui-1.10.4.custom.min.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
        <script type="text/javascript">
            $(function() {
                $("#tabs").tabs();
            });
        </script>
    </head>
    <body>
        <div class="container">
            <div class="row header">
                <div class="col-md-4 company">
                    <span class="company-name">Kernel Coders Lab</span><br>
                    <span class="company-title">We design and develop for you and for our eternal peace!</span>
                </div>
                <div class="col-md-8 header-right">
                    <div class="row">
                        <div class="col-md-6 search">
                            <form action="#" method="post">
                                <input type="text" name="search_item" size="30" placeholder="search" class="search-fld">
                                <input type="image" src="<?php echo base_url(); ?>/assets/img/search-icon.png" class="search-submit-btn">
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row menu-div-tag">
                <ul class="menu-ul">
                    <li id="home" onClick="populateContent(this)" class="menu active-menu"><a href="home">Home</a></li>
                    <li id="about" onClick="populateContent(this)" class="menu"><a href="about">About</a></li>
                    <li id="services" onClick="populateContent(this)" class="menu"><a href="services">Services</a></li>
                    <li id="portfolio" onClick="populateContent(this)" class="menu"><a href="portfolio">Portfolio</a></li>
                    <li id="blog" onClick="populateContent(this)" class="menu" ><a href="#">Blog</a></li>
                    <li id="contact" onClick="populateContent(this)" class="menu" ><a href="contact">Contact</a></li>
                </ul>
            </div>