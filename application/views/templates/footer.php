<div class="row footer">
    <div class="col-md-3 copy-right">
        <div class="row footer-heading">COPYRIGHT</div>
        <div class="row copy-right-text">kcl @ 2014</div>
    </div>
    <div class="col-md-5 about-us">
        <div class="row footer-heading">ABOUT US</div>
        <div class="row about-us-title">WE HELP COMPANIES MAKE GREAT SOFTWARE
            ON TIME AND AT LOWER COST</div>
        <div class="row about-us-text">Kernel Coders Lab is a Rajshahi,Bangladesh based software outsourcing company that focuses on highly qualitative, timely delivered and cost-effective offshore software development. 
        </div>
    </div>
    <div class="col-md-3 col-md-offset-1 follow-us">
        <div class="row footer-heading">FOLLOW US</div>
        <div class="row social-link">
            <ul>
                <li><a href="#"><img src="<?php echo base_url(); ?>/assets/img/facebook.png" alt="Facebook" title="facebook"></a></li>
                <li><a href="#"><img src="<?php echo base_url(); ?>/assets/img/twitter.png" alt="Twitter" title="twitter"></a></li>
                <li><a href="#"><img src="<?php echo base_url(); ?>/assets/img/google.png" alt="Google" title="google+"></a></li>
            </ul>
        </div>
    </div>
</div>
</div>
<script src="<?php echo base_url(); ?>/assets/js/library/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/dev/global.js"></script>
</body>
</html>