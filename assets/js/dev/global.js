/**Document Ready**/
$(function() {
    $('.menu').removeClass('active-menu');
    $('#' + menuId).addClass('active-menu');
});
/**Initialize Google Map**/
function initialize() {
    var map_canvas = document.getElementById('map_canvas');
    var map_options = {
        center: new google.maps.LatLng(24.379695, 88.598796),
        zoom: 8,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    var map = new google.maps.Map(map_canvas, map_options);
}
if (menuId == 'contact') {
    google.maps.event.addDomListener(window, 'load', initialize);
}
/**Set state active after populate content**/
function populateContent(ths) {
    var id = ths.id;
    $('.menu').removeClass('active-menu');
    $('#' + id).addClass('active-menu');
}

/**Send email**/

$('#sendEmail').click(function(e) {
    e.preventDefault();
    var emailForm = $('#emailForm');
    var url = $(emailForm).attr('action');
    var data = $(emailForm).serialize();
    $.ajax({
        url: url,
        data: data,
        type: 'POST',
        success: function(response) {

        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError);
        }

    });

});

function popupPortfolio(ths) {
    var html = "";
    var head = "";
    var url = $("#id" + ths).attr('url');
    var id = {id: ths};
    $.ajax({
        url: url,
        data: id,
        type: 'POST',
        dataType: 'json',
        success: function(response) {
            console.log(response['projects'])
            var pData = response['projects'];
            head = pData.project_name;
            html = '<div><div>Project Name :' + pData.project_name + '</div>'
                    + '<div> Language: ' + pData.project_lang + '</div> <div> Framework: ' + pData.project_frame + '</div>'
                    + '<div> Description :' + pData.project_description + '</div>'
                    + '<div> Remarks :' + pData.project_remarks + '</div></div>';
            $('#modalBody').html(html);
            $("#myModalLabel").html(head);
        },
        error: function(xhr, ajaxOptions, thrownError) {
            $('#modalBody').html(thrownError);
        }
    });
    $("#myModal").modal('show');

}


function populateHomeContent(ths) {
    var html = "";
    var head = "";
    if (ths.id == 'values') {
        html = '<div class="row amv-content">' +
                '<b>Long success: </b>We seek long-term success for our organization by bringing in success to our stakeholders and team members, as well as contributing to the up gradation of the society we live in. <br>' +
                '<b>Dignity: </b>Integrity, Honor and Pride. We honor our Commitments. We value individuals and their rights to express disagreement.<br>' +
                '<b>Customers Focus: </b>We deliver measurable Business Value. We bring greater flexibility, faster time to market, technical expertise and lower cost benefit to the doorstep of our customers.<br>' +
                '<b>Solution Orientation:</b>We treat each problem as a first step towards finding a solution. When faced with a problem we focus on a solution to the problem. <br>' +
                '<b>Communicate: </b>Communicating and expressing satisfaction & frustrations is the key to building teams and relationships.<br>' +
                '<b>Decision Making: </b>We believe Decisions are never right – we take them and then work very hard to make them right.<br>' +
                '<b>Quality Focus: </b>We make quality a value driver in our work, our products and our interactions. We do it “First Time Right”.<br></div>';
        head = 'VALUES'
        $('#modalBody').html(html);
        $("#myModalLabel").html(head);

    }
    else if (ths.id == 'mission') {
        
         html = '<div class="row amv-content">' +
                'Our mission today is made up of our vision, values, business strategy and brand promise that all come together to support '+
                'our purpose as a company; to work with our customers and partners to shape the future of travel; to deliver the best possible'+
                'reliable software and web solutions to help our clients by improving their IT efficiency and business profitability.</div>';
        head = 'MISSION'
        $('#modalBody').html(html);
        $("#myModalLabel").html(head);

    }
    $("#myModal").modal('show');
}
